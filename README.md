# Areas of Improvements
List of things the can be betterized.

## Documentation
- [ ] location/format
- [ ] teach seminar on git

## Devops
- [ ] infrastructure as code
- [ ] contiuous intergration
- [ ] app to safely read Cleveron office DB (In Progress)

## Monitoring
- [ ] zabbix alerts in Slack
- [ ] zabbix checks on critial infrastructure
- [ ] need a way to dynamically manage info on robots
- [ ] internal status.bhc.com page
- [ ] ELK stack

## Scalibility
- [ ] database HA
- [ ] azure autoscaling

## QA
- [ ] lint puppet
- [ ] automated testing
- [ ] load testing of BHC

## Infrastructure
- [ ] DNS in BHC
- [ ] GitHub/GitLab over bitbucket
- [ ] TLS management [Lemur](http://lemur.readthedocs.io/en/latest/)

## Operations
- [ ] Slack chat (In Progress)
